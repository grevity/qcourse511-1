<!-- The deadline is Dec 8 (AoE) -->

# Project Title 
<!-- Title format: "Project_Title | Team_Name (if applicable)" -->

### Keyword(s)
<!-- What keywords or domains would you assign this project to be under? -->

### Member(s) <!-- You can work individually or team up -->
<!-- Indicate full name, discord username, and git username (if applicable) -->

## Project Description
<!-- A few paragraphs describing your project, motivation, what it aims for and any relevant information about it. --> 
  
### Target Outcome
<!-- What's the expected result of your project? -->
  
### Midterm Deliverables
<!-- A short list of deliverables to be completed and reported by Dec 26 -->
  
### Final Deliverables
<!-- A short list of all deliverables to be completed and reported by Jan 9 -->

<!-- End of Submission. Scroll above and fill up the sections as per the description -->

<!-- FYI (Grading rubrics)
######################
- The project (outcomes and success of deliverables) is 20 points.
- The project report is 5 points
- Presentation is 5 points.
######################
-->

<!-- End of Template -->
