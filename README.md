<img src="https://qworld.net/wp-content/uploads/2021/10/QCourse511_www.jpg">

# QCourse511-1

We use this repo to keep track of term projects of [QCourse511-1](https://qworld.net/qcourse511-1/), which is open only for eligible students of QCourse 511-1. 

For each project, an issue is created on this repo by following [our template](.gitlab/issue_templates/submission-template.md).  
We use commenting tools for the updates and communications.  
We use [Markdown](https://www.markdownguide.org/basic-syntax/) for formating the text.

Please read the whole document before asking any questions.

You can use the dedicated channels on the course Discord server for your questions.

## Grading

Each project is graded out of 30 points:

- The project (outcomes and success of deliverables) is 20 points.
- The project report is 5 points
- Presentation is 5 points.

## Deadlines (AoE)

The deadlines are firm.

| Date   |  Task | if missed? |
| ------ | ------- | ---------- |
| **Wed, Dec 8** | Submission of project proposals | -3 points |
| Sat, Dec 11 | Late submission of project proposals | -30 points |
| Sun, Dec 12 | Revision of proposals if needed |  |
| **Sun, Dec 26** | Submission of midterm deliverables | -3 points |
| **Sun, Jan 9** | Submission of all deliverables | -3 points |
| **Tue, Jan 11** | Submission of presentations | -3 points |
| Fri, Jan 14 | Late submission of deliverables | -30 points |

The evaluation starts on Jan 12.   
As a part of the evaluation, verbal exams may be scheduled for some projects.

## Generic information and rules

The full name of each participant must be provided in the project documents.

### Project repo

Each project should have its public-access repo (Github, Gitlab, or similar).
  
The project developed for (or potentially developed for) QWorld may use a branch of this repo.  
_Specify this detail (if applicable) in the project proposal._  

Contact us if you think the nature of your project needs private access.  
_In this case, QCourse511 team should access the repo._


### Licensing of the projects

The project details and outcomes developed during QCourse511-1 should be publicly available, and they should have open-source licenses such as 
[Apache License](http://www.apache.org/licenses/LICENSE-2.0) or 
[MIT license](https://opensource.org/licenses/MIT) or 
[CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/legalcode) or 
[Educational Community License](https://opensource.org/licenses/ECL-2.0).

After QCourse511-1, the licensing of the new features or developments are up to the project member(s).

Contact us if you think the nature of your project needs some other types of licensing. We may consider exceptions after evaluating the request within our team.

## Submission of project proposals

Each project proposal is submitted either by an individual or by a group of students.  
In the latter case, a single submission should be made by listing each project member explicitly.

Depending on the nature project, the proposal by a group may be asked to go with individual projects.

The submission of a project is made by creating an issue on this repo.  
When creating an issue, please use our template.

### The structure of an issue

Any proposal should be submitted by Dec 8 with the following details:
- Project title
- Keywords
- Member(s)
- Project description
- Target outcome
- Midterm deliverables
- Final  deliverables

## Status 

We use several keywords to trace the status of projects:
- incomplete (-30 points)
- on-time-proposal
- late-proposal (-3 points)
- in-progress 
- mid-report
- missing-mid-report (-3 points)
- on-time-submission
- late-submission (-3 points)
- on-time-presentation
- late-presentation (-3 points)

These keywords will be assigned by our team.

## How to submit

Keep in your mind that the timestamps are visible and traceable on Gitlab.

The submissions will be made by updating or commenting the issues.  
The project team will check and verify the updates.

### Project proposal

The submission of a project proposal is done, once an issue is created and the required information is provided.

Do not make any further updates after the deadline until the team confirms the submission.

### Midterm deliverables

The midterm report is submitted as a comment of the issue before the deadline.

The details of the report should be verifiable. Otherwise, the report may be evaluated as incomplete.

_There is no late submission option for midterm reports._

### All deliverables

The final report is submitted as a comment of the issue before the deadline.

The details of the report should be verifiable. Otherwise, the report may be evaluated as incomplete.

Do not make any updates after the deadline.

### Presentation(s)

The presentation must be a video recording up to 10 minutes to explain the project details, deliverables and completed tasks, outcomes, and any other related details.
The achievements are more important than the other details.  
Do not exceed 10 minutes. The exceeding part may be discarded during the evaluation.

If it is a group project, then each member should prepare a separate video for up to 3 minutes explaining her contributions.

Do your submission as a comment of the issue before the deadline.

## Project ideas

You have one month to complete your project, and you can always continue your project after QCourse.  
Therefore, adjust your expectations and deliverables accordingly.

### Do not

If you have some attractive ideas but do not know what to do, then do not pick them for your term project.

If you want to learn some topics as a part of the project but do not have any pre-knowledge, then do not pick them for your term project.

### Do

If you have some pre-knowledge on a topic but do not know what to do, then 
- preparing an introductory tutorial with similar format of Bronze or Silver or
- creating a quantum game about it 
may be a good strategy.

### Some ideas from QJam2021

You can check [the project ideas prepared for QJam2021](https://gitlab.com/qworld/qjam/2021/-/blob/main/Project_Ideas.md).

You can check [the projects proposed during QJam2021](https://gitlab.com/qworld/qjam/2021/-/issues).

You may check other quantum jams-hackathons or quantum challenges.

You may check the online talks during QCourse511.

### Bottom line

If you do not have any idea, you can implement the project "Your Quantum Simulator" given in our tutorial Bronze-Qiskit.

You can use any programming language that you are comfortable with.
You may revise the project with new ideas.

## Weekly meetings

After recieving project proposals, we will schedule weekly online meetings with the team to answer your questions.

The details will be announced on Discord.